#ifndef ESTIM_H
#define ESTIM_H

typedef struct {
  unsigned long histogram;
  unsigned long pixels;
} estimate_stat;

void estimate_init (estimate_stat *s);
void process_blocks (estimate_stat *s, u8 *data, int width, int channels);
int estimate_quality (estimate_stat *s);

#endif

